# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ogatstyle/version'

Gem::Specification.new do |spec|
  spec.name          = "ogatstyle"
  spec.version       = Ogatstyle::VERSION
  spec.authors       = ["Elliot Bowes", "Tom Crouch"]
  spec.email         = ["e.bowes@outwood.com"]

  spec.summary       = %q{Provides SASS stylesheets for the OGAT Style.}
  spec.description   = %q{Provides SASS stylesheets for the OGAT Style.}
  spec.homepage      = "https://bitbucket.org/ogat/ogat-style"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.11"
  spec.add_development_dependency "rake", "~> 11.2"
  spec.add_development_dependency "rspec", "~> 3.2"
  spec.add_dependency 'rails', '>= 4.1'
  spec.add_dependency 'sass-rails', '>= 3.2'
  spec.add_dependency 'bootstrap-sass', '~> 3.3.0'
  spec.add_dependency 'font-awesome-sass', '~> 4.2'
  spec.add_dependency 'sprockets-rails', '>= 3'
end
