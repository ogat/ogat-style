## development

## v3.2.1 - 2016-10-3

- [OS-15] Fixed: correct asset names added to app.config.assets.precompile

## v3.2.0 - 2016-09-27

- [OS-12] Adjusted app title focus style to match hover state for web apps
- [OS-14] Changed shade of purple to match OGAT branding
- [OS-35] Ported changes identified in CON

## v3.1.1 - 2016-06-23

- [OC-11] Adjusted heading margin
- [OC-10] Adjusted brand spacing in webapp header
- [OC-9] Added a 2x image for webapp header icon

## v3.1.0 - 2016-06-21

- [OC-5] & [OC-6] Added full-width flat variant for web apps
- [OC-7] Added purple background style

## v3.0.0 - 2016-06-14

- Added filetype icon assets
- Bootstrap styles are no longer imported twice
- Fix to prevent the top of headings inside media-body elements being cut off
- [OS-2] Removed background image; GillSans is no longer included by default
- [OS-3] Added font GillSansLight

## v2.2.0 - 2014-12-09

- Move assets from vendor and add an initializer

## v2.1.3 - 2014-11-18

- Set sass-rails version to be at least 3.2

## v2.1.2 - 2014-11-04

- Include font-awesome-sprockets to ensure font awesome functions with Rails 4

## v2.1.1 - 2014-11-04

- Include bootstrap-sprockets to ensure icon fonts function

##v2.1.0 - 2014-11-04

- Merged in changes from tcrouch
- Upgraded to Bootstrap 3.3.0
- Upgraded to FontAwesome 4.2.0
- Upgraded sass-rails to 4.0.4

## v2.0.3 - 2014-02-14

- Upgraded to Bootstrap 3.1.1
- Upgraded to FontAwesome 4.0.2
