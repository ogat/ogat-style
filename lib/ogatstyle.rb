require 'ogatstyle/version'
require 'ogatstyle/fileicon'

module Ogatstyle
  module Rails
    class Engine < ::Rails::Engine
      require 'bootstrap-sass'
      require 'font-awesome-sass'

      initializer 'ogatstyle.assets.precompile' do |app|
        app.config.assets.precompile << 'ogatstyle/swirl-white-nav.png'
        app.config.assets.precompile << 'ogatstyle/swirl-white-nav2x.png'
        Fileicon::MAPPING.values.uniq.each do |ext|
          app.config.assets.precompile << "fileicon/#{ext}.png"
        end
      end
    end
  end
end
