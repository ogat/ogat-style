require 'ogatstyle/fileicon/mapping'

module Ogatstyle
  # Provides lookup of file type icon assets
  module Fileicon
    # Returns an icon to represent the given file
    # @param name_or_path [String] file name or path
    # @return (see #from_extension)
    def self.from_filename(name_or_path)
      Ogatstyle::Fileicon.from_extension File.extname(name_or_path)
    end

    # Returns an icon to represent the given file extension
    # @param extension [String] file extension (initial period optional)
    # @return [String] path to icon asset
    def self.from_extension(extension)
      extension ||= ''
      extension.downcase!
      extension = extension[1..-1] if extension[0, 1] == '.'

      "fileicon/#{MAPPING.fetch extension, 'default'}.png"
    end
  end
end
