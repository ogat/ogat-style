module Ogatstyle
  module Fileicon
    MAPPING = {
      'bmp'  => 'image',
      'gif'  => 'image',
      'jpeg' => 'image',
      'jpg'  => 'image',
      'png'  => 'image',
      'svg'  => 'image',
      'tiff' => 'image',

      '7z'   => 'archive',
      'bz2'  => 'archive',
      'gz'   => 'archive',
      'rar'  => 'archive',
      'sit'  => 'archive',
      'sitx' => 'archive',
      'tar'  => 'archive',
      'tgz'  => 'archive',
      'zip'  => 'archive',
      'zipx' => 'archive',

      'css' => 'css',

      'htm'  => 'html',
      'html' => 'html',

      'xls'  => 'excel',
      'xlsx' => 'excel',

      'ppt'  => 'powerpoint',
      'pptx' => 'powerpoint',

      'doc'  => 'word',
      'docx' => 'word',

      'pdd' => 'photoshop',
      'psb' => 'photoshop',
      'psd' => 'photoshop',

      'ai'  => 'illustrator',
      'pdf' => 'pdf',
      'swf' => 'flash',
      'txt' => 'text',

      'aac'  => 'music',
      'aif'  => 'music',
      'aiff' => 'music',
      'flac' => 'music',
      'm4a'  => 'music',
      'm4p'  => 'music',
      'mp3'  => 'music',
      'ogg'  => 'music',
      'vob'  => 'music',
      'wav'  => 'music',
      'wma'  => 'music',

      'avi'  => 'movie',
      'divx' => 'movie',
      'm4v'  => 'movie',
      'mkv'  => 'movie',
      'mov'  => 'movie',
      'mpeg' => 'movie',
      'mpg'  => 'movie'
    }.freeze
  end
end
