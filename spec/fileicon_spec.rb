RSpec.describe Ogatstyle::Fileicon do
  describe '#from_filename' do
    it 'extracts the extension from the given filename' do
      expect(File).to receive(:extname).with('file.ext').and_return('ext')
      Ogatstyle::Fileicon.from_filename 'file.ext'
    end

    it 'uses #from_extension to' do
      allow(File).to receive(:extname).and_return('foo')
      expect(Ogatstyle::Fileicon).to receive(:from_extension).with('foo')
      Ogatstyle::Fileicon.from_filename 'file.ext'
    end
  end

  describe '#from_extension' do
    it "returns a path to word.png for 'doc'" do
      expect(Ogatstyle::Fileicon.from_extension('doc')).to include('word.png')
    end

    it "returns a path to word.png for 'DOC'" do
      expect(Ogatstyle::Fileicon.from_extension('doc')).to include('word.png')
    end

    it "returns a path to word.png for '.doc'" do
      expect(Ogatstyle::Fileicon.from_extension('.doc')).to include('word.png')
    end

    it "returns a path to word.png for '.DOC'" do
      expect(Ogatstyle::Fileicon.from_extension('.DOC')).to include('word.png')
    end

    it 'returns a path to default.png for unknown file extensions' do
      expect(Ogatstyle::Fileicon.from_extension('.qwerty'))
        .to include('default.png')
    end
  end
end
